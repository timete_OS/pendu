import random
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy import Column, Integer, String
from config import Base


class Game(Base):
    __tablename__ = 'game'
    id = Column(Integer, primary_key=True, nullable=False)
    word = Column(String(50))
    life = Column(Integer)
    hint = Column(String(50))

    def __init__(self, user_id:str) -> None:
        self.id = user_id
        self.word = random.choice(['apocope',
                                'antonomase',
                                'uchronie',
                                'verge'])
        self.life = 8
        self.hint = self.word[0] + '_'*(len(self.word) - 1)

    def play(self, guess) -> bool:
        if guess == self.word:
            return True
        else:
            self.life -= 1
            new_hint = ''
            for index in range(len(self.word)):
                b = (guess == self.word[index])
                a = (self.hint[index] == '_')
                new_hint +=  (a and b)*self.word[index] + (1 - b)*self.hint[index]
            self.hint = new_hint
        return False
        