from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Integer, String
from sqlalchemy.ext.declarative import declarative_base



engine = create_engine('sqlite:///:memory:',echo=True)
connection = engine.connect()
Session = sessionmaker(bind=engine)
Base = declarative_base()
    