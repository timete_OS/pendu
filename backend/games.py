from game import Game
from config import Session


class Games:

    def __init__(self) -> None:
        self.all_games = dict()
        

    def get_current_game(self, id):
        if id in self.all_games.keys():
            return self.all_games[id]
        else:
            return False

    def create_game(self, id):
        session = Session()
        session
        self.all_games[id] = Game(id)
    

