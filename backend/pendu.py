from fastapi import FastAPI, Request
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

from games import Games
from game import Game

app = FastAPI()

all_games = Games()


@app.get("/play")
async def play(guess: str, request: Request):
    """allow to make a guess on a play

    Args:
        guess (str): the letter or word to be tested
        request (Request): info from the client

    Returns:
        json: the new hint or status of the game
    """
    ip = request.client.host
    g = all_games.get_current_game(ip)
    if not(g.play(guess)):
        if g.life == 0:
            return {"status" : "you died"}
        return {"life" : g.life, "word" : g.hint}
    return {"status" : "you won"}


@app.get("/start_game")
async def start_game(request: Request):
    """create a new game for a specific client

    hopefuly it will store it on the database

    Args:
        request (Request): info from the client

    Returns:
        json: status of the request ; need to handle exceptions
    """
    ip = request.client.host
    all_games.create_game(ip)
    return {"status" : "fine"}



@app.put("/config")
def config(params:str):
    # one day we may need to configure things
    if params:
        return {"copy": "we'll take care of that"}
    return
