Jouer au jeu
=====================

# Pour l'instant, il n'y a pas d'interface, YOLO 

## Lancer une partie

```sh
curl get http://127.0.0.1:8000/start_game | json_pp
```

## Jouer un coup

```sh
curl get http://127.0.0.1:8000/game?guess=m | json_pp
```

Response should be somehow similar to a json like this :

```json
{
   "life" : 5,
   "word" : "an__n_____"
}
```

or a word

```sh
curl get http://127.0.0.1:8000/game?guess=atavisme | json_pp
```