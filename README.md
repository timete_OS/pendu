Pendu
=====================

## What is Pendu ?

A great game about information theory.

## Why is Pendu ?

Because we're all going to die.

## Installation

Install fastapi, and others requirements.

```sh
pip install -r requirements.txt
```

Install the ASGI server uvicorn.

```sh
pip install "uvicorn[standard]"
```


According to the FastAPI doc, it is very straitforward : navigate to the Pendu directory and execute :

```sh
Bruce@BatDesktop:~/pendu/backend$ uvicorn pendu:app
```

In the case of a windows machine you need to execute inside bash :

```bash
Robin@WindowsMachine:~/pendu/backend$ python -m uvicorn pendu:app
```

Bravo ! Now the API is up and running, and ready to accept requests !


## Usage

Just use <code>curl</code> like the real chad you are.

### First you need to create your game

```sh
curl get http://127.0.0.1:8000/start_game | json_pp
```

### And then just play by giving your guess

It can be a letter

```sh
curl get http://127.0.0.1:8000/game?guess=m | json_pp
```

Response should be somehow similar to a json like this :

```json
{
   "life" : 5,
   "word" : "an__n_____"
}
```

or a word

```sh
curl get http://127.0.0.1:8000/game?guess=atavisme | json_pp
```